package abstracts;

public abstract class Lampara{

    protected String tamano;
    protected String pilas;
    protected int nivel;


    public Lampara(String tamano, String pilas, int nivel){

        this.tamano = tamano;
        this.pilas = pilas;
        this.nivel = nivel;

    }

    public abstract void mostrarCaracteristicas();


}

