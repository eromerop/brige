package herramientas;

import abstracts.Lampara;

public class LamparaSolar extends Lampara {

    StringBuffer salida = new StringBuffer();

    public LamparaSolar(String tamano, String pilas, int nivel){

        super(tamano, pilas, nivel);
    }

    @Override
    public void mostrarCaracteristicas(){

        salida.append("abstracts.Lampara tamaño");
        salida.append(tamano);
        salida.append("de pilas");
        salida.append(pilas);
        salida.append("de potencia");
        salida.append(nivel);


    }
}
