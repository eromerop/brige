package herramientas;

import abstracts.Lampara;

public class LamparaLuz extends Lampara {

    StringBuffer salida = new StringBuffer();

    public LamparaLuz(String tamano,String pilas,int nivel){

        super(tamano,pilas,nivel);
    }

    @Override
    public void mostrarCaracteristicas(){

        salida.append("tamaño es");
        salida.append(tamano);
        salida.append("de pilas");
        salida.append(pilas);
        salida.append("nivel es");
        salida.append(nivel);

    }
}
